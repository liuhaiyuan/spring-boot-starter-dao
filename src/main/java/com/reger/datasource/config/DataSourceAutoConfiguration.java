package com.reger.datasource.config;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.ibatis.session.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.Ordered;
import org.springframework.core.env.Environment;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

//import com.reger.datasource.aspect.DataSourceAspect;
import com.reger.datasource.core.AbstractDataBaseBean;
import com.reger.datasource.core.Mapper;
import com.reger.datasource.core.Order;
import com.reger.datasource.properties.DaoProperties;
import com.reger.datasource.properties.DruidProperties;
import com.reger.datasource.properties.MybatisNodeProperties;

import io.shardingjdbc.core.constant.DatabaseType;
import tk.mybatis.mapper.code.Style;

@EnableTransactionManagement(proxyTargetClass = true, order = Ordered.HIGHEST_PRECEDENCE)
public class DataSourceAutoConfiguration extends AbstractDataBaseBean
		implements BeanDefinitionRegistryPostProcessor, EnvironmentAware {

	static Logger log = LoggerFactory.getLogger(DataSourceAutoConfiguration.class);

	Environment environment;

	@Override
	public void setEnvironment(Environment environment) {
		this.environment = environment;
	}

	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
	}

	@Override
	public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
		DaoProperties druidConfig = ConfigUtils.getDruidConfig(environment, DaoProperties.dbprefix,
				DaoProperties.class);

		DruidProperties defaultConfig = ConfigUtils.getDruidConfig(environment, DruidProperties.druidDefault,
				DruidProperties.class);

		Configuration configuration = druidConfig.getConfiguration();
		Map<String, MybatisNodeProperties> druidNodeConfigs = druidConfig.getNodes();
		if (druidNodeConfigs == null || druidNodeConfigs.isEmpty()) {
			throw new RuntimeException("至少需要配置一个DataBase(配置DataBase参数在" + DaoProperties.dbprefix + ".nodes)");
		}
		Iterator<Entry<String, MybatisNodeProperties>> it = this.setPrimary(druidNodeConfigs).entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, MybatisNodeProperties> entry = (Map.Entry<String, MybatisNodeProperties>) it.next();
			String druidNodeName = entry.getKey();
			MybatisNodeProperties druidNodeConfig = entry.getValue();
			try {
				Configuration _configuration = super.cloneConfiguration(configuration);
				this.registryBean(druidNodeName, druidNodeConfig, defaultConfig, _configuration, registry);
			} catch (Throwable e) {
				throw new RuntimeException(e);
			}
		}
	}

	private Map<String, MybatisNodeProperties> setPrimary(Map<String, MybatisNodeProperties> druidNodeConfigs) {
		int primarys = 0;
		MybatisNodeProperties defDruidNode = null;
		for (Entry<String, MybatisNodeProperties> entry : druidNodeConfigs.entrySet()) {
			MybatisNodeProperties druidNode = entry.getValue();
			if (druidNode != null && druidNode.isPrimary()) {
				primarys++;
				if (primarys > 1)
					druidNode.setPrimary(false);
			}
			if (druidNode != null && defDruidNode == null) {
				defDruidNode = druidNode;
			}
		}
		if (primarys == 0 && defDruidNode != null)
			defDruidNode.setPrimary(true);
		return druidNodeConfigs;
	}

	private void registryBean(String druidNodeName, MybatisNodeProperties nodeProperties,
			DruidProperties defaultProperties, Configuration configuration, BeanDefinitionRegistry registry) {
		if (nodeProperties == null) {
			return;
		}
		Assert.notEmpty(nodeProperties.getDataSources(), "数据源不可以为空");
		String mapperPackage = nodeProperties.getMapperPackage();
		String typeAliasesPackage = nodeProperties.getTypeAliasesPackage();
		DatabaseType databaseType = super.getDbType(
				nodeProperties.getDataSources().values().iterator().next().getMaster(), defaultProperties);
		Order order = nodeProperties.getOrder();
		Style style = nodeProperties.getStyle();
		Mapper mappers = Mapper.valueOfDialect(databaseType);
		String basepackage = nodeProperties.getBasePackage();
		if (StringUtils.isEmpty(basepackage)) {
			log.warn("BasePackage为空，db配置异常,当前配置数据源对象的名字{}", druidNodeName);
			basepackage = "";
		}
		boolean primary = nodeProperties.isPrimary();
		String dataSourceName = druidNodeName + "DataSource";
		String jdbcTemplateName = druidNodeName + "JdbcTemplate";
		String transactionManagerName = druidNodeName;
		String sqlSessionFactoryBeanName = druidNodeName + "RegerSqlSessionFactoryBean";
		String scannerConfigurerName = druidNodeName + "RegerScannerConfigurer";

		AbstractBeanDefinition dataSource = super.createDataSource(nodeProperties, defaultProperties, dataSourceName);
		AbstractBeanDefinition jdbcTemplate = super.createJdbcTemplate(dataSourceName);
		AbstractBeanDefinition transactionManager = super.createTransactionManager(dataSourceName);

		AbstractBeanDefinition sqlSessionFactoryBean = super.createSqlSessionFactoryBean(dataSourceName, mapperPackage,
				typeAliasesPackage, databaseType, configuration);
		AbstractBeanDefinition scannerConfigurer = super.createScannerConfigurerBean(sqlSessionFactoryBeanName,
				basepackage, mappers, order, style, nodeProperties.getProperties());

		dataSource.setLazyInit(true);
		dataSource.setPrimary(primary);
		dataSource.setScope(BeanDefinition.SCOPE_SINGLETON);
		jdbcTemplate.setLazyInit(true);
		jdbcTemplate.setPrimary(primary);
		jdbcTemplate.setScope(BeanDefinition.SCOPE_SINGLETON);
		transactionManager.setLazyInit(true);
		transactionManager.setPrimary(primary);
		transactionManager.setScope(BeanDefinition.SCOPE_SINGLETON);
		sqlSessionFactoryBean.setLazyInit(true);
		sqlSessionFactoryBean.setPrimary(primary);
		sqlSessionFactoryBean.setScope(BeanDefinition.SCOPE_SINGLETON);
		scannerConfigurer.setLazyInit(true);
		scannerConfigurer.setPrimary(primary);
		scannerConfigurer.setScope(BeanDefinition.SCOPE_SINGLETON);

		registry.registerBeanDefinition(dataSourceName, dataSource);
		registry.registerBeanDefinition(jdbcTemplateName, jdbcTemplate);
		registry.registerBeanDefinition(transactionManagerName, transactionManager);
		registry.registerBeanDefinition(sqlSessionFactoryBeanName, sqlSessionFactoryBean);
		registry.registerBeanDefinition(scannerConfigurerName, scannerConfigurer);

		if (primary) {
			registry.registerAlias(dataSourceName, "dataSource");
			registry.registerAlias(jdbcTemplateName, "jdbcTemplate");
			registry.registerAlias(transactionManagerName, "transactionManager");
		}
	}

//	@Bean
//	public DataSourceAspect dataSourceAspect() {
//		return new DataSourceAspect();
//	}
}
